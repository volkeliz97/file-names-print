import java.io.File;

public class Main {
    public static void main(String[] args) {
        printFileNames("./");
    }

    public static void printFileNames(String path) {
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                System.out.println(listOfFiles[i].getName());
            }
        }
    }
}
